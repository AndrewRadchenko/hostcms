# Чиcтый проект hostcms

Релиз очищен от лишних сущностей, удалены все стандартные шаблоны, страницы и данные.

Предназначен для развертки проектов с нуля.

## Установка

Разверните у себя проект

Скачайте [дамп базы данных](https://www.dropbox.com/s/0l1py3tt8ukgbkl/dump2018-07-26-17.22.05.sql?dl=0)

Скачайте [пример конфига](https://www.dropbox.com/s/388kytp0j74ib9s/database.php?dl=0) базы данных и разместите его в директории /modules/core/config/

Добавьте свою лицензию hostcms(логин, номер лицензии и пинкод) в админке в разеделе /система/константы

HOSTCMS_PIN_CODE				
HOSTCMS_CONTRACT_NUMBER				
HOSTCMS_USER_LOGIN

Перенесите [содержимое](https://www.dropbox.com/sh/ncu0u6fcf6a2gda/AADd9CWn7_bP7PC6RpIKAdHMa?dl=0) в директорию uploads

[Скачать все вспомогательные файлы разом](https://www.dropbox.com/sh/22dq7s8bj76sw01/AAD0U5uQ4loxrULNJZg-2zRda?dl=0)

